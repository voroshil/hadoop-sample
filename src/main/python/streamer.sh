#!/bin/sh

if test -n "$5"
then
#JAVA_ARGS="$JAVA_ARGS -D mapred.reduce.tasks=$5"
JAVA_ARGS="$JAVA_ARGS -D stream.num.map.output.key.fields=$5"
fi

echo "Using input $1"
echo "Using output $2"
echo "Using mapper $3"
echo "Using reducer $4"
echo "Additional args: $JAVA_ARGS"

${HADOOP_PREFIX}/bin/hadoop jar ${HADOOP_PREFIX}/share/hadoop/tools/lib/hadoop-streaming-3.1.2.jar $JAVA_ARGS \
    -input $1 \
    -output $2 \
    -mapper $3 \
    -reducer $4

