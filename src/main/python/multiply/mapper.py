#!/usr/bin/python

import sys


current = 0

for line in sys.stdin:
  line = line.strip()
  name,val = line.split(',', 1)

  try:
    val = float(val)
  except ValueError:
    continue


  for i in range(0, 10):
    print "%d\t%d\t%d\t%s\t%d" %(current, i, 0, name,val)
    print "%d\t%d\t%d\t%s\t%d" %(i, current, 1, name,val)

  current = current + 1
"""
0,0,0,a0
0,0,1,a0
0,1,0,a0
1,0,1,a0
0,2,0,a0
2,0,1,a0

1,0,0,a1
1,1,0,a1
1,2,0,a1
0,1,1,a1
1,1,1,a1
2,1,1,a1

2,0,0,a2
2,1,0,a2
2,2,0,a2
0,2,1,a2
1,2,1,a2
2,2,1,a2
"""