#!/usr/bin/python

import sys


def func(a,b):
  return a*b

first_name = None

for line in sys.stdin:
  line = line.strip()
  (n1,n2,n3,name,val) = line.split('\t', 4)

  try:
    val = float(val)
  except ValueError:
    continue

  if n3 == "0":
    first_name = name
    first_val = val
  elif n3 == "1" and first_name is not None:
    second_name = name
    second_val = val
    print "%d\t%d\t%d\t%s*%s\t%d" %(0,0,3,first_name,second_name,func(first_val, second_val))
  else:
    continue
    