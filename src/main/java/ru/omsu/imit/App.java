package ru.omsu.imit;
import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;

public class App{
    public static void ls(String hdfsUrl, String root, boolean recursive) throws Exception{
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", hdfsUrl);
        FileSystem fs = FileSystem.get(configuration);
        Stack<String> directories = new Stack<>();
        directories.push(hdfsUrl + root);
        while(!directories.isEmpty()) {
            String currentPath = directories.pop();
            FileStatus[] status = fs.listStatus(new Path(currentPath));  // you need to pass in your hdfs path

            for (int i = 0; i < status.length; i++) {
                if (status[i].isDirectory()){
                    directories.push(status[i].getPath()+"/");
                    System.out.println(status[i].getPath()+"/");
                }else{
                    System.out.println(status[i].getPath());
                }
            }
        }
    }
    public static void cat(String hdfsUrl, String path) throws IOException {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", hdfsUrl);
        FileSystem fs = FileSystem.get(configuration);
        Path p = new Path(hdfsUrl, path);
        FileStatus status = fs.getFileStatus(p);
        if (status.isDirectory()){
            System.out.println("Given path is directory. Use ls for listing");
            return;
        }
        BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status.getPath())));
        String line;
        line=br.readLine();
        while (line != null){
            System.out.println(line);
            line=br.readLine();
        }
    }
    public static void main (String [] args) throws Exception{
        String url = args[0];
        System.out.println(url);
//        try{
        //ls("hdfs://192.168.99.100:8020", "/", false);
//        }catch(Exception e){
//            e.printStackTrace();
//            System.out.println("File not found");
//        }
    }
}